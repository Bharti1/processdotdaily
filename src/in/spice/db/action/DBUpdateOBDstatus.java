package in.spice.db.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import in.spice.db.DBManager;
import in.spice.pojo.DotDailyPojo;

public class DBUpdateOBDstatus {

	public void updateOBDstatus(List<DotDailyPojo> dotDailyList){
		Connection con = null;
		PreparedStatement pStmt = null;
		int batchSize = 999;
		int counter = 0;
		try {
			DBManager objDBManager = new DBManager();
			String query = "UPDATE dot_obd_dump SET obd_status='1'  WHERE msisdn = ?;";
			con = objDBManager.getConnection();
			pStmt = con.prepareStatement(query);
			for (int i = 0; i < dotDailyList.size(); i++) {
		//		pStmt.setString(1, dotDailyList.get(i).getMsisdn());
				pStmt.setString(1, dotDailyList.get(i).getMobileNum());
				
				pStmt.addBatch();
				if (++counter % batchSize == 0) {
					pStmt.executeBatch();
					System.out.println("Batch uploaded.....");
					pStmt.clearParameters();
				}
			}
			pStmt.executeBatch();
			pStmt.clearParameters();
			System.out.println("Update Obd_Status in dump db successfully.....");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(con);
			DbUtils.closeQuietly(pStmt);
		}
		
		
	}
}
