package in.spice.db.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.dbutils.DbUtils;

import com.mysql.jdbc.Util;

import in.spice.db.DBManager;
import in.spice.pojo.DotDailyPojo;
import in.spice.util.Utilities;


public class DBProcessDot {
	
	public List<DotDailyPojo> getDailyList(List<DotDailyPojo> dotDailylist){

		Connection con = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		 System.out.println("base value : "+new Utilities().getDays());
		try {
			DBManager objDBManager = new DBManager();
			String query = "SELECT quera.operator , quera.lsa , quera.msisdn ,lang,city,postal_code,term_cell , quera.RowNumber_a FROM "
							+"(SELECT operator , lsa , msisdn , lang,city,postal_code,term_cell ,RowNumber_a FROM"
							+"(SELECT operator , lsa , msisdn , lang,city,postal_code,term_cell ,"
							+"@row_num := IF(@prev_value=CONCAT(operator,lsa) ,@row_num+1,1) AS RowNumber_a"
							+",@prev_value := CONCAT(operator,lsa) FROM "
							+"(SELECT DISTINCT(msisdn),operator,lsa,lang,city,postal_code,term_cell FROM dot_obd_dump  WHERE obd_status ='0') a ,  "
							+ "(SELECT @row_num := 1) b ,(SELECT @prev_value := '') c "
							+"ORDER BY operator , lsa , 4 ) inner_quer) quera ,"
							+"(SELECT ROUND(COUNT(1)/?) cnt, operator,lsa FROM dot_obd_dump WHERE obd_status ='0' GROUP BY operator,lsa) querb "
							+"WHERE quera.operator = querb.operator AND quera.lsa = querb.lsa "
							+"AND   quera.RowNumber_a<=cnt ORDER BY  operator , lsa , RowNumber_a";
			
			con = objDBManager.getConnection();
			pStmt = con.prepareStatement(query);
			
					pStmt.setInt(1, new Utilities().getDays());
					rs = pStmt.executeQuery();
					
					while(rs.next()){
						DotDailyPojo dotDailyPojo = new DotDailyPojo();
				 
						    dotDailyPojo.setOperator(rs.getString(1));
						    dotDailyPojo.setLsa(rs.getString(2));
							dotDailyPojo.setMsisdn(rs.getString(3));
							dotDailyPojo.setLang(rs.getString(4));
							dotDailyPojo.setCity(rs.getString(5));
							dotDailyPojo.setPostalCode(rs.getString(6));
							dotDailyPojo.setFileName(rs.getString(7));
							
							//System.out.println("Operator "+ rs.getString(1)+"& Lsa "+rs.getString(2)+"& Msisdn "+rs.getString(3)+"& Lang "+rs.getString(4)+"& City "+rs.getString(5)+"& PostalCode "+rs.getString(6)+"& fileName "+rs.getString(7));
				   
							dotDailylist.add(dotDailyPojo);
					}
				
			System.out.println("list "+ dotDailylist.size());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(con);
			DbUtils.closeQuietly(pStmt);
		}
		
		return dotDailylist;
		
	}
	
	
}
