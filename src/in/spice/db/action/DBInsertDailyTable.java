package in.spice.db.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import in.spice.db.DBManager;
import in.spice.pojo.DotDailyPojo;

public class DBInsertDailyTable {

	public boolean DailyTableInsert(List<DotDailyPojo> dotDailyList)
	{   boolean status = false;
		Connection con = null;
		PreparedStatement pStmt = null;
		int batchSize = 999;
		int counter = 0;
		try {
			DBManager objDBManager = new DBManager();
			String query = "insert into dot_obd_daily(msisdn,operator,lsa,lang,dnd_status,city,postal_code,term_cell,status,calling_datetime) values(?,?,?,?,?,?,?,?,?,NOW())";
			con = objDBManager.getConnection();
			pStmt = con.prepareStatement(query);
			for (int i = 0; i < dotDailyList.size(); i++) {
				pStmt.setString(1, dotDailyList.get(i).getMsisdn());
				pStmt.setString(2, dotDailyList.get(i).getOperator());
				pStmt.setString(3, dotDailyList.get(i).getLsa());
				pStmt.setString(4, dotDailyList.get(i).getLang().toLowerCase());
				pStmt.setString(5, dotDailyList.get(i).getDndStatus());
				pStmt.setString(6, dotDailyList.get(i).getCity());
				pStmt.setString(7, dotDailyList.get(i).getPostalCode());
				pStmt.setString(8, dotDailyList.get(i).getFileName());
				pStmt.setString(9, "I");
				
				pStmt.addBatch();
				if (++counter % batchSize == 0) {
					pStmt.executeBatch();
					System.out.println("Batch uploaded.....");
					pStmt.clearParameters();
				}
			}
			pStmt.executeBatch();
			status = true;
			pStmt.clearParameters();
			System.out.println("Insert in daily db successfully.....");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(con);
			DbUtils.closeQuietly(pStmt);
		}
		
		
		return true;
		
	}
}
