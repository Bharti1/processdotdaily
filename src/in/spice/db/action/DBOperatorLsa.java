package in.spice.db.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.dbutils.DbUtils;

import in.spice.db.DBManager;
import in.spice.util.Utilities;

public class DBOperatorLsa {

	public Map<String, Integer> getOpeartor() {
		Connection con = null;
		
		PreparedStatement pStmt1 = null;
		ResultSet rs1 = null;
		
		
		 List<String> operatorlist = getOperatorList();
       
        Map<String, Integer> operatorMap = new HashMap<String, Integer>();

       
		try {
			DBManager objDBManager = new DBManager();
			con = objDBManager.getConnection();
			
			 
			
//			 for(int i = 0; i < operatorlist.size(); i++)
//			 {
//			 System.out.println(operatorlist.get(i)+" : "+ (lsaListMap.get(operatorlist.get(i))).size());
//			 }
//			 for(int i = 0; i < operatorlist.size(); i++)
//			 {
//				 System.out.println(operatorlist.get(i));
//			 }
			 
			 // list print
			
			 
			 for(int i = 0; i < operatorlist.size(); i++)
			 {
				
				 String query1 = "SELECT count(1) FROM dot_obd_dump where operator = ? AND obd_status = '0'";
					pStmt1 = con.prepareStatement(query1);
					pStmt1.setString(1, operatorlist.get(i));
					 rs1 = pStmt1.executeQuery();
					 while(rs1.next()){
						 operatorMap.put(operatorlist.get(i), rs1.getInt(1));
					 }
				 
			 }
			 // map print
			// System.out.println(operatorMap.size());
			 System.out.println(operatorMap);
			 
//			 Calendar objCalendar=Calendar.getInstance();
//				int day=objCalendar.get(Calendar.DAY_OF_MONTH);
//				int totalDayInMonth=objCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
//				//System.out.println(totalDayInMonth+" : "+day);
//				int baseValue=(totalDayInMonth-day);
	
			 int baseValue=new Utilities().getDays();
			 System.out.println("baseValue: "+baseValue);
			 
			 for(int i = 0; i < operatorMap.size(); i++)
			 {
				 int count = 0;
				 int netCount = operatorMap.get(operatorlist.get(i));
				// System.out.println("netcount : "+netCount);
				 
				 if(netCount > baseValue){
				 count = (netCount)/ baseValue;
				 operatorMap.put(operatorlist.get(i), count);
				 //System.out.println("operator: "+ operatorlist.get(i)+" count"+count);

				 }
				 else
				 {
					 operatorMap.put(operatorlist.get(i), netCount);
					// System.out.println("operator: "+ operatorlist.get(i)+" count"+count);
				 }
			 
			 }
			 
			 System.out.println("Operaters "+operatorMap);
			 
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(con);
			DbUtils.closeQuietly(pStmt1);
		}
		return operatorMap;
	}
	
	public List<String> getOperatorList()
	{
		 List<String> operatorlist = new  ArrayList<String>();
		 
		 Connection con = null;
		 PreparedStatement pStmt = null;
			ResultSet rs = null;
			try {
				DBManager objDBManager = new DBManager();
				
				String query = "SELECT DISTINCT(operator) FROM dot_obd_dump WHERE obd_status = '0'";
				con = objDBManager.getConnection();
				pStmt = con.prepareStatement(query);
				 rs = pStmt.executeQuery();
				 while(rs.next()){
				   operatorlist.add(rs.getString(1));
				 }
				 
					 System.out.println("operator numbers : "+operatorlist.size());
					
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				DbUtils.closeQuietly(con);
				DbUtils.closeQuietly(pStmt);
			}
		 
		return operatorlist;
		
	}
	
	public Map<String, Integer> getLsaCount()
	{
        Map<String, Integer> countLsaMap= new HashMap<String, Integer>();
        List<String> operatorlist = getOperatorList();
        Connection con = null;
        PreparedStatement pStmt2 = null;
		ResultSet rs2 = null;
		try {
			DBManager objDBManager = new DBManager();
			 con = objDBManager.getConnection();
			 for(int i = 0; i < operatorlist.size(); i++){
				 
				 String queryLsaCount = "SELECT count(DISTINCT(lsa)) FROM dot_obd_dump WHERE operator = ? AND obd_status = '0'";
					
				 pStmt2 = con.prepareStatement(queryLsaCount);
					pStmt2.setString(1, operatorlist.get(i));
					 rs2 = pStmt2.executeQuery();
					 while(rs2.next()){
						 countLsaMap.put(operatorlist.get(i), rs2.getInt(1));
					 }
				 }
				 System.out.println("operator numbers : "+operatorlist.size());
				 System.out.println("circle count : "+countLsaMap);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(con);
			DbUtils.closeQuietly(pStmt2);
		}
        
		return countLsaMap;
		
	}
	
//	public Map<String, List<String>> getOperatorLsa()
//	{
//		 Map<String,List<String>> lsaListMap = new HashMap<String,List<String>>() ;
//        List<String> operatorlist = getOperatorList();
//        Connection con = null;
//        PreparedStatement pStmt3 = null;
//		ResultSet rs3 = null;
//		Connection pStmt2;
//		try {
//			DBManager objDBManager = new DBManager();
//			 con = objDBManager.getConnection();
//			 for(int i = 0; i < operatorlist.size(); i++)
//			 {
//			 
//				// int count = (int) countLsaMap.get(operatorlist.get(i));
////				 String querylsaList = "SELECT DISTINCT(lsa) FROM dot_obd_dump where operator = ? AND obd_status ='0'";
////				 pStmt1 = con.prepareStatement(querylsaList);
////				 pStmt1.setString(1, operatorlist.get(i));
////				 rs3 = pStmt1.executeQuery();
////				 List<String> lsalistobj = new ArrayList<String>();
////
////				 while(rs3.next()){
////					 String value = rs3.getString(1);
////					 lsalistobj.add(value);
////				
//				 }
//				 
//				 
//				 
////				 lsaListMap.put(operatorlist.get(i), lsalistobj);
////				// System.out.println("size "+lsalistobj.size());
////				// lsalistobj.clear();
////			 }
////			 
////			 for (Entry<String, List<String>> ee : lsaListMap.entrySet()) {
////				    String key = ee.getKey();
////				    List<String> values = ee.getValue();
////				    System.out.println("key : "+key+" values : "+values);
////				    
////				    // TODO: Do something.
////				}
////			 
////		} catch (Exception e) {
////			e.printStackTrace();
////		} finally {
////			DbUtils.closeQuietly(con);
////			DbUtils.closeQuietly(pStmt2);
////		}
////        
//		return lsaListMap;
//		
//	}
//	
	
}
