package in.spice.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class Utilities {
	private static Properties objProperties;

	public static String getConfKeyValue(String key) throws IOException {
		FileInputStream fis = null;
		try {
			String filePath = "";
			if (objProperties == null) {
				objProperties = new Properties();
				if (OsType.isWindows()) {
					filePath = "D:/dot.properties";
				} else {
					filePath = "/home/DOT_OBD_Process/dot.properties";
				}
				fis = new FileInputStream(filePath);
				objProperties.load(fis);
			}
		} catch (Exception e) {
			System.out.println("Error while loading Property file:" + e.getMessage());
		} finally {
			if (fis != null) {
				fis.close();
			}
		}
		return objProperties.getProperty(key);
	}

	public static void reload() {
		objProperties = null;
	}

	public static String getFileExtension(String fileName) {
		String extension = "";
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			extension = fileName.substring(fileName.lastIndexOf(".") + 1);
		return extension;
	}

	public int getDays() {
		int baseValue = 0;
		try {
			int diffDays = 0;
			int totalNumDay = 0;
			String processStartDate = Utilities.getConfKeyValue("process.data.start.date");
			String totalNumberOfDays = Utilities.getConfKeyValue("process.data.total.days");
			if (totalNumberOfDays != null && !"".equals(totalNumberOfDays)) {
				totalNumDay = Integer.parseInt(totalNumberOfDays);
			}
			Date objCurDate = new Date();
			DateFormat objDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			String curDate = objDateFormat.format(objCurDate);
			Date d1 = null;
			Date d2 = null;
			d1 = objDateFormat.parse(processStartDate);
			d2 = objDateFormat.parse(curDate);
			long diff = d2.getTime() - d1.getTime();
			diffDays = (int) (diff / (24 * 60 * 60 * 1000));
			baseValue = (totalNumDay - diffDays);
			if (baseValue == 0) {
				baseValue = 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return baseValue;
	}

}