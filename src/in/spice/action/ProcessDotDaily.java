package in.spice.action;

import java.io.BufferedReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.IOUtils;

import in.spice.db.action.DBProcessDot;
import in.spice.db.action.DBUpdateOBDstatus;
import in.spice.db.action.DBInsertDailyTable;
import in.spice.pojo.DotDailyPojo;

public class ProcessDotDaily {

	public static void main(String[] args) {
		List<DotDailyPojo> dotDailyList = new ArrayList<DotDailyPojo>();
		ProcessDotDaily processDotDailyobj = new ProcessDotDaily();
		String dndStatus = "NO";
		int count = 0;

		DBProcessDot db = new DBProcessDot();
		dotDailyList = db.getDailyList(dotDailyList);
		// if(dotDailyList!=null && dotDailyList.size() > 0)

		System.out.println("List size: "+dotDailyList.size());
		if (dotDailyList != null && dotDailyList.size() > 0) {
			System.out.println("DND_Status Check start");
			
			
			for (int i = 0; i < dotDailyList.size(); i++) {
				String mobileNo = null;
				if (!dotDailyList.get(i).getMsisdn().isEmpty()) {
					mobileNo = dotDailyList.get(i).getMsisdn();
					
				//	String targetURL ="http://192.168.9.128:8080/DNDfilter/CheckDND?msisdn="+dotDailyList.get(i).getMsisdn()+"&username=DOT_OBD";
					

			String targetURL = "http://103.15.179.31:8080/DNDfilter/CheckDND?msisdn="+dotDailyList.get(i).getMsisdn() + "&username=DOT_OBD";

					dndStatus = processDotDailyobj.executePost(targetURL);
					dotDailyList.get(i).setDndStatus(dndStatus.trim());
					dotDailyList.get(i).setMobileNum(mobileNo);
					
					
					if(mobileNo.length() > 10){
						mobileNo = mobileNo.substring(mobileNo.length()-10);
						dotDailyList.get(i).setMsisdn(mobileNo);
						System.out.println("Mobile:  "+dotDailyList.get(i).getMsisdn());
					}
					
					count = i;
				

					if (count % 1000 == 0) {
						System.out.println("msisdn= " + dotDailyList.get(i).getMsisdn() + " status= " + dndStatus
								+ " count= " + count);
					}

				}

			}
			System.out.println("DND_Status Check finish");
		}

		DBInsertDailyTable dbdaily = new DBInsertDailyTable();
		boolean udateDailyCheck = dbdaily.DailyTableInsert(dotDailyList);
		// if(udateDailyCheck)
		if (udateDailyCheck) {
			DBUpdateOBDstatus dbObd = new DBUpdateOBDstatus();
			dbObd.updateOBDstatus(dotDailyList);
		}

	}

	private String executePost(String targetURL) {
		URL url = null;
		URLConnection conn = null;
		InputStream is = null;
		BufferedReader rd = null;
		StringBuilder response = new StringBuilder();
		try {
			url = new URL(targetURL);
			conn = url.openConnection();
			is = conn.getInputStream();
			rd = new BufferedReader(new InputStreamReader(is));
			String line;
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(rd);
			
			
			
			
		}
		return response.toString();
	}

}
